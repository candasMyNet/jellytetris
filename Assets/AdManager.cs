﻿using System.Collections.Generic;
using UnityEngine;

namespace Funpac
{
    public class AdManager : MonoBehaviour
    {
        #region Singleton

        private static AdManager _instance;

        public static AdManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<AdManager>();
                }

                return _instance;
            }
        }
        #endregion
        
        
        // State maps to enable/disable GUI ad state buttons
        private readonly Dictionary<string, bool> _adUnitToLoadedMapping = new Dictionary<string, bool>();

        private readonly Dictionary<string, bool> _adUnitToShownMapping = new Dictionary<string, bool>();

        private readonly Dictionary<string, List<MoPub.Reward>> _adUnitToRewardsMapping =
            new Dictionary<string, List<MoPub.Reward>>();

        private bool _consentDialogLoaded;

#if UNITY_IOS
        private readonly string _bannerAdUnit = "7cd322abcd73440a8f0bcb98305c88a8";

        private readonly string _interstitialAdUnit = "fb7044921cda4935952985e8cd3b322a";

        private readonly string _rewardedVideoAdUnit = "953f22ccf48c4bf785501ce1e50436d5";

        private readonly string[] _rewardedRichMediaAdUnits = { };
#elif UNITY_ANDROID || UNITY_EDITOR
    private readonly string[] _bannerAdUnits = { "b195f8dd8ded45fe847ad89ed1d016da" };
    private readonly string[] _interstitialAdUnits = { "24534e1901884e398f1253216226017e" };
    private readonly string[] _rewardedVideoAdUnits = { "920b6145fb1546cf8b5cf2ac34638bb7" };
    private readonly string[] _rewardedRichMediaAdUnits = { "a96ae2ef41d44822af45c6328c4e1eb1" };
#endif
        // Buffer space between sections
        private int _sectionMarginSize;

        // Scroll view position
        // private Vector2 _scrollPosition;

        // Label style for plugin and SDK version banner
        private GUIStyle _centeredStyle;

        // Default text for custom data fields
        private static string _customDataDefaultText = "Optional custom data";

        // String to fill with custom data for Rewarded Videos
        private string _rvCustomData = _customDataDefaultText;

        // String to fill with custom data for Rewarded Rich Media
        private string _rrmCustomData = _customDataDefaultText;

        // Flag indicating that personally identifiable information can be collected
        private bool _canCollectPersonalInfo = false;

        // Current consent status of this user to collect personally identifiable information
        private MoPub.Consent.Status _currentConsentStatus = MoPub.Consent.Status.Unknown;

        // Flag indicating that consent should be acquired to collect personally identifiable information
        private bool _shouldShowConsentDialog = false;

        // Flag indicating that the General Data Protection Regulation (GDPR) applies to this user
        private bool? _isGdprApplicable = false;

        // Flag indicating that the General Data Protection Regulation (GDPR) has been forcibly applied by the publisher
        private bool _isGdprForced = false;

        // Index for current banner ad position, which is incremented after every banner request, starting with BottomCenter
        // private int _bannerPositionIndex = 5;

        // All possible banner positions
        // private readonly MoPub.AdPosition[] _bannerPositions =
        //     Enum.GetValues(typeof(MoPub.AdPosition)).Cast<MoPub.AdPosition>().ToArray();

        private static bool IsAdUnitArrayNullOrEmpty(ICollection<string> adUnitArray)
        {
            return (adUnitArray == null || adUnitArray.Count == 0);
        }


        private void AddAdUnitsToStateMaps(string adUnit)
        {
            _adUnitToLoadedMapping[adUnit] = false;
            _adUnitToShownMapping[adUnit] = false;
        }


        public void SdkInitialized()
        {
            UpdateConsentValues();
        }


        public void ConsentStatusChanged(MoPub.Consent.Status oldStatus, MoPub.Consent.Status newStatus,
            bool canCollectPersonalInfo)
        {
            _canCollectPersonalInfo = canCollectPersonalInfo;
            _currentConsentStatus = newStatus;
            _shouldShowConsentDialog = MoPub.ShouldShowConsentDialog;
        }

        public void LoadAvailableRewards(string adUnitId, List<MoPub.Reward> availableRewards)
        {
            // Remove any existing available rewards associated with this AdUnit from previous ad requests
            _adUnitToRewardsMapping.Remove(adUnitId);

            if (availableRewards != null)
            {
                _adUnitToRewardsMapping[adUnitId] = availableRewards;
            }
        }


        public void BannerLoaded(string adUnitId, float height)
        {
            AdLoaded(adUnitId);
            _adUnitToShownMapping[adUnitId] = true;
        }


        public void AdLoaded(string adUnit)
        {
            _adUnitToLoadedMapping[adUnit] = true;
            // UpdateStatusLabel("Loaded " + adUnit);
        }


        public void AdDismissed(string adUnit)
        {
            _adUnitToLoadedMapping[adUnit] = false;
            // ClearStatusLabel();
        }

        public void ImpressionTracked(string adUnit, MoPub.ImpressionData impressionData)
        {
            // UpdateStatusLabel("Impression tracked for " + adUnit + " with impression data: "
            //                   + impressionData.JsonRepresentation);
        }

        public bool ConsentDialogLoaded
        {
            private get => _consentDialogLoaded; 
            set
            {
                _consentDialogLoaded = value;
                // if (_consentDialogLoaded) UpdateStatusLabel("Consent dialog loaded");
            }
        }

        private void Awake()
        {
            AddAdUnitsToStateMaps(_bannerAdUnit);
            AddAdUnitsToStateMaps(_interstitialAdUnit);
            AddAdUnitsToStateMaps(_rewardedVideoAdUnit);
            // AddAdUnitsToStateMaps(_rewardedRichMediaAdUnit);
            
            ConsentDialogLoaded = false;
        }

        private void Start()
        {
            // The SdkInitialize() call is handled by the MoPubManager prefab now. Please see:
            // https://developers.mopub.com/publishers/unity/initialize/#option-1-use-the-mopub-manager-recommended

            MoPub.LoadBannerPluginsForAdUnits(_bannerAdUnit);
            MoPub.LoadInterstitialPluginsForAdUnits(_interstitialAdUnit);
            MoPub.LoadRewardedVideoPluginsForAdUnits(_rewardedVideoAdUnit);
            // MoPub.LoadRewardedVideoPluginsForAdUnits(_rewardedRichMediaAdUnits);

#if !(UNITY_ANDROID || UNITY_IOS)
        Debug.LogError("Please switch to either Android or iOS platforms to run sample app!");
#endif

#if UNITY_EDITOR
            Debug.LogWarning("No SDK was loaded since this is not on a mobile device! Real ads will not load.");
#endif
            var nativeAdsGO = GameObject.Find("MoPubNativeAds");
            if (nativeAdsGO != null)
                nativeAdsGO.SetActive(false);
        }

        // private void Update()
        // {
            // // Enable scrollview dragging
            // foreach (var touch in Input.touches)
            // {
            //     if (touch.phase != TouchPhase.Moved) continue;
            //     _scrollPosition.y += touch.deltaPosition.y;
            //     _scrollPosition.x -= touch.deltaPosition.x;
            // }
        // }

        // private void OnGUI()
        // {
//             GUI.skin = _skin;
//
// #if UNITY_2017_3_OR_NEWER
//             // Screen.safeArea was added in Unity 2017.2.0p1
//             var guiArea = Screen.safeArea;
// #else
//         var guiArea = new Rect(0, 0, Screen.width, Screen.height);
// #endif
            // guiArea.x += 20;
            // guiArea.y += 20;
            // guiArea.width -= 40;
            // guiArea.height -= 40;
            // GUILayout.BeginArea(guiArea);
            // _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);

            // CreateTitleSection();
            // CreateBannersSection();
            // CreateInterstitialsSection();
            // CreateRewardedVideosSection();
            // CreateRewardedRichMediaSection();
            
            // CreateUserConsentSection();
            // CreateActionsSection();
            // CreateStatusSection();

            // GUILayout.EndScrollView();
            // GUILayout.EndArea();
        // }

        public void HideBanner()
        {
            MoPub.ShowBanner(_bannerAdUnit, false);
            _adUnitToShownMapping[_bannerAdUnit] = false;
        }

        public void ShowBanner()
        {
            MoPub.ShowBanner(_bannerAdUnit, true);
            _adUnitToShownMapping[_bannerAdUnit] = true;
        }

        public void DestroyBanner()
        {
            MoPub.DestroyBanner(_bannerAdUnit);
            _adUnitToLoadedMapping[_bannerAdUnit] = false;
            _adUnitToShownMapping[_bannerAdUnit] = false;
        }

        public void RequestBanner()
        {
            var position = MoPub.AdPosition.BottomCenter;//_bannerPositions[_bannerPositionIndex++];
            // UpdateStatusLabel(string.Format("Requesting {0} at position {1}", bannerAdUnit, position));
            MoPub.RequestBanner(_bannerAdUnit, position, MoPub.MaxAdSize.Width336Height280);
            // _bannerPositionIndex %= _bannerPositions.Length;
        }

        public void DestroyInterstitial()
        {
            MoPub.DestroyInterstitialAd(_interstitialAdUnit);
            _adUnitToLoadedMapping[_interstitialAdUnit] = false;
        }

        public void ShowInterstitial()
        {
            MoPub.ShowInterstitialAd(_interstitialAdUnit);
        }

        public void RequestInterstitial()
        {
            Debug.Log("requesting interstitial with AdUnit: " + _interstitialAdUnit);
            // UpdateStatusLabel("Requesting " + interstitialAdUnit);
            MoPub.RequestInterstitialAd(_interstitialAdUnit);
        }


        private void CreateRewardedVideosSection()
        {
            // GUILayout.Space(_sectionMarginSize);
            // GUILayout.Label("Rewarded Videos");
            // if (!IsAdUnitArrayNullOrEmpty(_rewardedVideoAdUnits))
            // {
                CreateCustomDataField("rvCustomDataField", ref _rvCustomData);
                // foreach (var _rewardedVideoAdUnit in _rewardedVideoAdUnits)
                // {
                    GUILayout.BeginHorizontal();

                    GUI.enabled = !_adUnitToLoadedMapping[_rewardedVideoAdUnit];
                    if (GUILayout.Button(CreateRequestButtonLabel(_rewardedVideoAdUnit)))
                    {
                        Debug.Log("requesting rewarded video with AdUnit: " + _rewardedVideoAdUnit);
                        // UpdateStatusLabel("Requesting " + _rewardedVideoAdUnit);
                        MoPub.RequestRewardedVideo(
                            adUnitId: _rewardedVideoAdUnit, keywords: "rewarded, video, mopub", customerId: "customer101");
                    }

                    GUI.enabled = _adUnitToLoadedMapping[_rewardedVideoAdUnit];
                    if (GUILayout.Button("Show"))
                    {
                        // ClearStatusLabel();
                        MoPub.ShowRewardedVideo(_rewardedVideoAdUnit, GetCustomData(_rvCustomData));
                    }

                    GUI.enabled = true;

                    GUILayout.EndHorizontal();


                    // // Display rewards if there's a rewarded video loaded and there are multiple rewards available
                    // if (!MoPub.HasRewardedVideo(_rewardedVideoAdUnit)
                    //     || !_adUnitToRewardsMapping.ContainsKey(_rewardedVideoAdUnit)
                    //     || _adUnitToRewardsMapping[_rewardedVideoAdUnit].Count <= 1) continue;

                    // GUILayout.BeginVertical();
                    // GUILayout.Space(_sectionMarginSize);
                    // GUILayout.Label("Select a reward:");

                    foreach (var reward in _adUnitToRewardsMapping[_rewardedVideoAdUnit])
                    {
                        if (GUILayout.Button(reward.ToString()))
                        {
                            MoPub.SelectReward(_rewardedVideoAdUnit, reward);
                        }
                    }
                    //
                    // GUILayout.Space(_sectionMarginSize);
                    // GUILayout.EndVertical();
                // }
            // }
            // else
            // {
            //     GUILayout.Label("No rewarded video AdUnits available", _smallerFont, null);
            // }
        }


        private void CreateRewardedRichMediaSection()
        {
            GUILayout.Space(_sectionMarginSize);
            GUILayout.Label("Rewarded Rich Media");
            if (!IsAdUnitArrayNullOrEmpty(_rewardedRichMediaAdUnits))
            {
                CreateCustomDataField("rrmCustomDataField", ref _rrmCustomData);
                foreach (var rewardedRichMediaAdUnit in _rewardedRichMediaAdUnits)
                {
                    GUILayout.BeginHorizontal();

                    GUI.enabled = !_adUnitToLoadedMapping[rewardedRichMediaAdUnit];
                    if (GUILayout.Button(CreateRequestButtonLabel(rewardedRichMediaAdUnit)))
                    {
                        Debug.Log("requesting rewarded rich media with AdUnit: " + rewardedRichMediaAdUnit);
                        // UpdateStatusLabel("Requesting " + rewardedRichMediaAdUnit);
                        MoPub.RequestRewardedVideo(
                            adUnitId: rewardedRichMediaAdUnit, keywords: "rewarded, video, mopub",
                            latitude: 37.7833, longitude: 122.4167, customerId: "customer101");
                    }

                    GUI.enabled = _adUnitToLoadedMapping[rewardedRichMediaAdUnit];
                    if (GUILayout.Button("Show"))
                    {
                        // ClearStatusLabel();
                        MoPub.ShowRewardedVideo(rewardedRichMediaAdUnit, GetCustomData(_rrmCustomData));
                    }

                    GUI.enabled = true;

                    GUILayout.EndHorizontal();

                    // Display rewards if there's a rewarded rich media ad loaded and there are multiple rewards available
                    if (!MoPub.HasRewardedVideo(rewardedRichMediaAdUnit)
                        || !_adUnitToRewardsMapping.ContainsKey(rewardedRichMediaAdUnit)
                        || _adUnitToRewardsMapping[rewardedRichMediaAdUnit].Count <= 1) continue;

                    GUILayout.BeginVertical();
                    GUILayout.Space(_sectionMarginSize);
                    GUILayout.Label("Select a reward:");

                    foreach (var reward in _adUnitToRewardsMapping[rewardedRichMediaAdUnit])
                    {
                        if (GUILayout.Button(reward.ToString()))
                        {
                            MoPub.SelectReward(rewardedRichMediaAdUnit, reward);
                        }
                    }

                    GUILayout.Space(_sectionMarginSize);
                    GUILayout.EndVertical();
                }
            }
            // else
            // {
            //     GUILayout.Label("No rewarded rich media AdUnits available", _smallerFont, null);
            // }
        }

        private void CreateUserConsentSection()
        {
            // GUILayout.Space(_sectionMarginSize);
            // GUILayout.Label("User Consent");
            // GUILayout.Label("Can collect personally identifiable information: " + _canCollectPersonalInfo,
            //     _smallerFont);
            // GUILayout.Label("Current consent status: " + _currentConsentStatus, _smallerFont);
            // GUILayout.Label("Should show consent dialog: " + _shouldShowConsentDialog, _smallerFont);
            // GUILayout.Label(
            //     "Is GDPR applicable: " + (_isGdprApplicable != null ? _isGdprApplicable.ToString() : "Unknown"),
            //     _smallerFont);
            //
            // GUILayout.BeginHorizontal();
            // GUI.enabled = !ConsentDialogLoaded;
            if (GUILayout.Button("Load Consent Dialog"))
            {
                // UpdateStatusLabel("Loading consent dialog");
                MoPub.LoadConsentDialog();
            }

            GUI.enabled = ConsentDialogLoaded;
            if (GUILayout.Button("Show Consent Dialog"))
            {
                // ClearStatusLabel();
                MoPub.ShowConsentDialog();
            }

            GUI.enabled = !_isGdprForced;
            if (GUILayout.Button("Force GDPR"))
            {
                // ClearStatusLabel();
                MoPub.ForceGdprApplicable();
                UpdateConsentValues();
                _isGdprForced = true;
            }

            GUI.enabled = true;
            if (GUILayout.Button("Grant Consent"))
            {
                MoPub.PartnerApi.GrantConsent();
            }

            if (GUILayout.Button("Revoke Consent"))
            {
                MoPub.PartnerApi.RevokeConsent();
            }

            // GUI.enabled = true;
            //
            // GUILayout.EndHorizontal();
        }


        private void CreateActionsSection()
        {
            GUILayout.Space(_sectionMarginSize);
            GUILayout.Label("Actions");
            if (GUILayout.Button("Report App Open"))
            {
                // ClearStatusLabel();
                MoPub.ReportApplicationOpen();
            }

            if (!GUILayout.Button("Enable Location Support")) return;

            // ClearStatusLabel();
            MoPub.EnableLocationSupport(true);
        }


        private void UpdateConsentValues()
        {
            _canCollectPersonalInfo = MoPub.CanCollectPersonalInfo;
            _currentConsentStatus = MoPub.CurrentConsentStatus;
            _shouldShowConsentDialog = MoPub.ShouldShowConsentDialog;
            _isGdprApplicable = MoPub.IsGdprApplicable;
        }


        private static void CreateCustomDataField(string fieldName, ref string customDataValue)
        {
            GUI.SetNextControlName(fieldName);
            customDataValue = GUILayout.TextField(customDataValue, GUILayout.MinWidth(200));
            if (Event.current.type != EventType.Repaint) return;
            if (GUI.GetNameOfFocusedControl() == fieldName && customDataValue == _customDataDefaultText)
            {
                // Clear default text when focused
                customDataValue = string.Empty;
            }
            else if (GUI.GetNameOfFocusedControl() != fieldName && string.IsNullOrEmpty(customDataValue))
            {
                // Restore default text when unfocused and empty
                customDataValue = _customDataDefaultText;
            }
        }


        private static string GetCustomData(string customDataFieldValue)
        {
            return customDataFieldValue != _customDataDefaultText ? customDataFieldValue : null;
        }


        private static string CreateRequestButtonLabel(string adUnit)
        {
            return adUnit.Length > 10 ? "Request " + adUnit.Substring(0, 10) + "..." : adUnit;
        }

    }
}