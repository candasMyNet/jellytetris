using System.Collections.Generic;

public class TapjoyPackageConfig : PackageConfig
{
    public override string Name
    {
        get { return "Tapjoy"; }
    }

    public override string Version
    {
        get { return /*UNITY_PACKAGE_VERSION*/"1.2.10"; }
    }

    public override Dictionary<Platform, string> NetworkSdkVersions
    {
        get {
            return new Dictionary<Platform, string> {
                { Platform.ANDROID, /*ANDROID_SDK_VERSION*/"12.4.0" },
                { Platform.IOS, /*IOS_SDK_VERSION*/"12.4.0" }
            };
        }
    }

    public override Dictionary<Platform, string> AdapterClassNames
    {
        get {
            return new Dictionary<Platform, string> {
                { Platform.ANDROID, "com.mopub.mobileads.Tapjoy" },
                { Platform.IOS, "Tapjoy" }
            };
        }
    }
}
