﻿#if mopub_manager
using UnityEngine;

public class TapjoyNetworkConfig : MoPubNetworkConfig
{
    public override string AdapterConfigurationClassName
    {
        get { return Application.platform == RuntimePlatform.Android
                  ? "com.mopub.mobileads.TapjoyAdapterConfiguration"
                  : "TapjoyAdapterConfiguration"; }
    }

    public override string MediationSettingsClassName
    {
        get { return Application.platform == RuntimePlatform.Android
                  ? "com.mopub.mobileads.TapjoyRewardedVideo$TapjoyMediationSettings"
                  : "TapjoyGlobalMediationSettings"; }
    }

    [Tooltip("Enter your SDK key to be used to initialize the Tapjoy SDK.")]
    [Config.Optional]
    public PlatformSpecificString sdkKey;

    public override MoPub.MediatedNetwork NetworkOptions
    {
        get {
            var options = base.NetworkOptions;

            // Copy the sdkKey into mediation settings as well.
            string key;
            if (options.NetworkConfiguration.TryGetValue("sdkKey", out key))
                options.MediationSettings["sdkKey"] = sdkKey;

            return options;
        }
    }
}
#endif

